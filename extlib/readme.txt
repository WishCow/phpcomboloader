This is the dir to store the YUI library files.

Directory structure:
extlib/ (here)
    3.8.0/
        build/
            align-plugin
            (...)
            yui
            yui-base
            yui-throttle
    yui3-gallery/
        build/
            gallery-a11ychecker-base
            gallery-a11ychecker-ui
            gallery-accordion
            (...)


Script to get the files:
wget http://yui.zenfs.com/releases/yui3/yui_3.8.0.zip
unzip yui_3.8.0.zip "yui/build/*"
rm yui_3.8.0.zip
mv "yui" "3.8.0"

wget https://github.com/yui/yui3-gallery/archive/master.zip
unzip master.zip "yui3-gallery-master/build/*"
rm master.zip
mv "yui3-gallery-master" "yui3-gallery"
