<?php

require_once dirname(__FILE__) . '/setup.php';

class CssTest extends PHPUnit_Framework_TestCase {
    protected function setUp() {
    }

    public function testBasic() {
        $content = <<<'EOF'
            test01:url();
            test02:url(../../../test02.gif);
            test03:url(http://bla/test03.gif);
            test04:url(https://bla/test04.gif);
            test05:url(data:base64,test05);
            test06:url(/test06.gif);
            test07:url(test07.gif);

EOF;

        $out = handle_yui_css($content, 'abc/build/def/ghi.css');
        $out = explode("\n", $out);

        $i = 0;
        $this->assertContains('test01:url()', $out[$i++]);
        $this->assertContains('test02:url(http://the.combo/url?abc/build/test02.gif)', $out[$i++]);
        $this->assertContains('test03:url(http://bla/test03.gif);', $out[$i++]);
        $this->assertContains('test04:url(https://bla/test04.gif);', $out[$i++]);
        $this->assertContains('test05:url(data:base64,test05);', $out[$i++]);
        $this->assertContains('test06:url(/test06.gif);', $out[$i++]);
        $this->assertContains('test07:url(http://the.combo/url?abc/build/def/test07.gif);', $out[$i++]);
    }
    public function testBasicSpace() {
        $content = <<<'EOF'
            test01:url(   );
            test02:url(  ../../../test02.gif );
            test03:url(  http://bla/test03.gif );
            test04:url(  https://bla/test04.gif );
            test05:url(  data:base64,test05 );
            test06:url(  /test06.gif );
            test07:url(  test07.gif );

EOF;

        $out = handle_yui_css($content, 'abc/build/def/ghi.css');
        $out = explode("\n", $out);

        $i = 0;
        $this->assertContains('test01:url(   )', $out[$i++]);
        $this->assertContains('test02:url(  http://the.combo/url?abc/build/test02.gif )', $out[$i++]);
        $this->assertContains('test03:url(  http://bla/test03.gif );', $out[$i++]);
        $this->assertContains('test04:url(  https://bla/test04.gif );', $out[$i++]);
        $this->assertContains('test05:url(  data:base64,test05 );', $out[$i++]);
        $this->assertContains('test06:url(  /test06.gif );', $out[$i++]);
        $this->assertContains('test07:url(  http://the.combo/url?abc/build/def/test07.gif );', $out[$i++]);
    }

    public function testSingleQuote() {
        $content = <<<'EOF'
            test01:url('');
            test02:url('../../../test02.gif');
            test03:url('http://bla/test03.gif');
            test04:url('https://bla/test04.gif');
            test05:url('data:base64,test05');
            test06:url('/test06.gif');
            test07:url('test07.gif');

EOF;

        $out = handle_yui_css($content, 'abc/build/def/ghi.css');
        $out = explode("\n", $out);

        $i = 0;
        $this->assertContains("test01:url('')", $out[$i++]);
        $this->assertContains("test02:url('http://the.combo/url?abc/build/test02.gif')", $out[$i++]);
        $this->assertContains("test03:url('http://bla/test03.gif');", $out[$i++]);
        $this->assertContains("test04:url('https://bla/test04.gif');", $out[$i++]);
        $this->assertContains("test05:url('data:base64,test05');", $out[$i++]);
        $this->assertContains("test06:url('/test06.gif');", $out[$i++]);
        $this->assertContains("test07:url('http://the.combo/url?abc/build/def/test07.gif');", $out[$i++]);
    }
    public function testSingleQuoteSpace() {
        $content = <<<'EOF'
            test01:url(  '' );
            test02:url(  '../../../test02.gif' );
            test03:url(  'http://bla/test03.gif' );
            test04:url(  'https://bla/test04.gif' );
            test05:url(  'data:base64,test05' );
            test06:url(  '/test06.gif' );
            test07:url(  'test07.gif' );


EOF;

        $out = handle_yui_css($content, 'abc/build/def/ghi.css');
        $out = explode("\n", $out);

        $i = 0;
        $this->assertContains("test01:url(  '' )", $out[$i++]);
        $this->assertContains("test02:url(  'http://the.combo/url?abc/build/test02.gif' )", $out[$i++]);
        $this->assertContains("test03:url(  'http://bla/test03.gif' );", $out[$i++]);
        $this->assertContains("test04:url(  'https://bla/test04.gif' );", $out[$i++]);
        $this->assertContains("test05:url(  'data:base64,test05' );", $out[$i++]);
        $this->assertContains("test06:url(  '/test06.gif' );", $out[$i++]);
        $this->assertContains("test07:url(  'http://the.combo/url?abc/build/def/test07.gif' );", $out[$i++]);
    }

    public function testDoubleQuote() {
        $content = <<<'EOF'
            test01:url("");
            test02:url("../../../test02.gif");
            test03:url("http://bla/test03.gif");
            test04:url("https://bla/test04.gif");
            test05:url("data:base64,test05");
            test06:url("/test06.gif");
            test07:url("test07.gif");

EOF;

        $out = handle_yui_css($content, 'abc/build/def/ghi.css');
        $out = explode("\n", $out);

        $i = 0;
        $this->assertContains('test01:url("")', $out[$i++]);
        $this->assertContains('test02:url("http://the.combo/url?abc/build/test02.gif")', $out[$i++]);
        $this->assertContains('test03:url("http://bla/test03.gif");', $out[$i++]);
        $this->assertContains('test04:url("https://bla/test04.gif");', $out[$i++]);
        $this->assertContains('test05:url("data:base64,test05");', $out[$i++]);
        $this->assertContains('test06:url("/test06.gif");', $out[$i++]);
        $this->assertContains('test07:url("http://the.combo/url?abc/build/def/test07.gif");', $out[$i++]);
    }

    public function testDoubleQuoteSpace() {
        $content = <<<'EOF'
            test01:url( ""  );
            test02:url( "../../../test02.gif"  );
            test03:url( "http://bla/test03.gif"  );
            test04:url( "https://bla/test04.gif"  );
            test05:url( "data:base64,test05"  );
            test06:url( "/test06.gif"  );
            test07:url( "test07.gif"  );


EOF;

        $out = handle_yui_css($content, 'abc/build/def/ghi.css');
        $out = explode("\n", $out);

        $i = 0;
        $this->assertContains('test01:url( ""  )', $out[$i++]);
        $this->assertContains('test02:url( "http://the.combo/url?abc/build/test02.gif"  )', $out[$i++]);
        $this->assertContains('test03:url( "http://bla/test03.gif"  );', $out[$i++]);
        $this->assertContains('test04:url( "https://bla/test04.gif"  );', $out[$i++]);
        $this->assertContains('test05:url( "data:base64,test05"  );', $out[$i++]);
        $this->assertContains('test06:url( "/test06.gif"  );', $out[$i++]);
        $this->assertContains('test07:url( "http://the.combo/url?abc/build/def/test07.gif"  );', $out[$i++]);
    }


}
