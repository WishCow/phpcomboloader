<?php
    require_once('../ini/conf.php');
?>
<!DOCTYPE html>
<html>
<head>
<base href="https://yuidemo.atomas.com/demo/">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <script type="text/javascript" src="<?php echo COMBO_URL ?>?3.8.1/build/yui/yui-min.js"></script>
    <script>
        var YUI_config={combine:true,comboBase:'<?php echo COMBO_URL ?>?',gallery:'yui3-gallery'};
    </script>
</head>
<body class="yui3-skin-sam">
<h1>Basic Sliders</h1>

<div class="exampleIntro">
    <p>This example walks you through the basics of creating a Slider from script.
Both Sliders in this example use the default value range of 0 - 100 and use
default skin resources provided with the Sam skin.</p>

<p>The first Slider is set up in a more traditional JavaScript coding style and
the second using the shorter method chaining style.  The first Slider is
configured to move along the vertical axis, and is initialized to a value of
30.  The second Slider is horizontal and instantiated with minimal
configuration.</p>
	    
</div>

<!--BEGIN SOURCE CODE FOR EXAMPLE =============================== -->

<div id="mycalendar">
</div>
<script type="text/javascript">
YUI().use('calendar', function (Y) {

  // Create a new instance of Calendar, setting its width 
  // and height, allowing the dates from the previous
  // and next month to be visible and setting the initial
  // date to be November, 1982.
  var calendar = new Y.Calendar({
          contentBox: "#mycalendar",
          height:'200px',
          width:'600px',
          showPrevMonth: true,
          showNextMonth: true,
          date: new Date(1982,11,1)}).render();

});
</script>

<!--END SOURCE CODE FOR EXAMPLE =============================== -->


</body>
</html>
