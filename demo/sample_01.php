<?php
    require_once('../ini/conf.php');

//https://yuidemo.atomas.com/combo.php?3.8.1/build/datatable-base/assets/skins/sam/datatable-base-skin.css
//https://yuidemo.atomas.com/combo.php?3.8.1/build/slider-base/assets/skins/sam/slider-skin.css
?>
<!DOCTYPE html>
<html>
<head>
<base href="https://yuidemo.atomas.com/demo/">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <script type="text/javascript" src="<?php echo COMBO_URL ?>?3.8.1/build/yui/yui-min.js"></script>
    <script>
        var YUI_config={combine:true,comboBase:'<?php echo COMBO_URL ?>?',gallery:'yui3-gallery'};
    </script>
</head>
<body class="yui3-skin-sam">
<h1>Basic Sliders</h1>

<div class="exampleIntro">
    <p>This example walks you through the basics of creating a Slider from script.
Both Sliders in this example use the default value range of 0 - 100 and use
default skin resources provided with the Sam skin.</p>

<p>The first Slider is set up in a more traditional JavaScript coding style and
the second using the shorter method chaining style.  The first Slider is
configured to move along the vertical axis, and is initialized to a value of
30.  The second Slider is horizontal and instantiated with minimal
configuration.</p>
	    
</div>

<!--BEGIN SOURCE CODE FOR EXAMPLE =============================== -->

<div id="demo">

    <h4>Vertical Slider</h4>
    <p id="vert_value">Value: 30</p>
    <div id="vert_slider"></div>

    <h4>Horizontal Slider</h4>
    <p id="horiz_value">Value: 0</p>
    <div class="horiz_slider"></div>

</div>
<script type="text/javascript">
// Create a YUI instance and request the slider module and its dependencies
YUI().use("slider", function(Y) {

// store the node to display the vertical Slider's current value
var v_report = Y.one('#vert_value'),
    vert_slider;
    
// instantiate the vertical Slider.  Use the classic thumb provided with the
// Sam skin
vert_slider = new Y.Slider({
    axis: 'y', // vertical Slider
    min: 0,
    max: 100,
    value: 30, // initial value
    railSize: '10em', // range the thumb can move through
    thumbImage: 'assets/images/thumbY.png'
});

// callback function to display Slider's current value
function reportValue(e) {
    v_report.set('innerHTML', 'Value: ' + e.newVal);
}

vert_slider.after('valueChange', reportValue);

// render the slider into the first element with class vert_slider
vert_slider.render('#vert_slider');



// instantiate the horizontal Slider, render it, and subscribe to its
// valueChange event via method chaining.  No need to store the created Slider
// in this case.

new Y.Slider({
        railSize: '200px',
        thumbImage: 'assets/images/thumbX.png'
    }).
    render('.horiz_slider').
    after('valueChange',function (e) {
        Y.one('#horiz_value').set('innerHTML', 'Value: ' + e.newVal);
    });

});

</script>

<!--END SOURCE CODE FOR EXAMPLE =============================== -->


</body>
</html>
