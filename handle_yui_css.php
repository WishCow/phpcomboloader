<?php
/**
 * transform YUI css url()
 * @param string $content
 * @param string file name ie "yui3-gallery/build/gallerycss-xarno-skins/gallerycss-xarno-skins.css"
 * @return string
 */
function handle_yui_css($content, $st) {
    // is YUI: need rewriting of the url() scheme
    $urlBase = COMBO_URL . "?" . substr($st, 0, strrpos($st, "/") + 1);                      // remove filename
    $urlRoot = COMBO_URL . "?" . substr($st, 0, strpos( $st, "/", strpos($st, "/")+1 ) + 1); // keep 2 levels

    // Handle image path corrections (order is important).
    // The following come from https://github.com/yui/phploader/blob/master/phploader/combo.php#L120

    // just filename or subdirs/filename (e.g) url(foo.png), url(foo/foo.png) : (don't match url beginning with http:// data: ../)
//    $content = preg_replace( '/((url\()((?!(http\:|data\:|\.\.\/|\/))\S+)(\)))/',
//                             '${2}' . $urlBase . '${3}${4})',
//                             $content );

//        $content = preg_replace( '/((url\()((?!(https?\:|data\:|\.\.\/|\/))\S+)(\)))/',
//                             '${2}' . $urlBase . '${3}${4})',
//                             $content );

//        $content = preg_replace( '/((url\(\s*)((?!(https?\:|data\:|\.\.\/|\/))\S+)(\s*\)))/',
//                             '${2}' . $urlBase . '${3}${4})',
//                             $content );

    $regexp =
        '(' .                                         // ${1}: opening part "url("  ie "url( '"
            'url\(\s*' .
            '([\'\"]?+)' .                            // ${2}: enventual opening quote
        ')' .
        '(' .                                         // ${3}: inside of url('abc'), without qhote
            '(?!(https?\:|data\:|\.\.\/|\/))' .       // ${4}: always empty !
            '\S+' .
        ')' .
        '(' .                                         // ${5}: closing part ie "')"
            '(\2)' .                                  // ${6}: same as ${2} : matching quote, if any
            '\s*\)'.
        ')'
    ;

    $content = preg_replace( "/$regexp/",
                             '${1}' . $urlBase . '${3}${5}',
                              $content );


    // slash filename (e.g.) url(/whatever): (Removed, there is no such scheme in YUI 3.8.1)
    //    $content = str_replace( "url(/",
    //                            "url($urlBase",
    //                            $content );

    // relative paths (e.g.) url(../../foo.png):
    $regexp =
        '(' .                                         // ${1}: opening part "url("  ie "url( '"
            'url\(\s*' .
            '([\'\"]?+)' .                            // ${2}: enventual opening quote
        ')' .
        '(\.\.\/)+'  .                                // ${3}: ../../../
        '(\S+)' .                                     // ${4}: main part
        '(' .                                         // ${5}: closing part ie "')"
            '(\2)' .                                  // ${6}: same as ${2} : matching quote, if any
            '\s*\)'.
        ')'
    ;
    $content = preg_replace( "/$regexp/",
                             '${1}' . $urlRoot . '${4}${5}',
                              $content );

    return $content;
}
