<?php
ini_set("display_errors", false);
error_reporting(0);

define("PCL_VERSION", "1.1.1");

if ((@include "ini/conf.php") !== 1) {
    die("ini/conf.php not found ! you must copy the file <tt>conf.php.dist</tt> to <tt>conf.php</tt> and customize it before using phpcomboloader.");
}

require "handle_yui_css.php";

// handle cache
if( isset($_SERVER["HTTP_IF_MODIFIED_SINCE"]) || isset($_SERVER["HTTP_IF_NONE_MATCH"])) {
    header("HTTP/1.0 304 Not Modified");
    exit();
}

define("YUI_ROOT", realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . "extlib") . DIRECTORY_SEPARATOR);

// get the requested files. If Path Info is available, use it, so the combo loader can be called with
// http://.../combo.php/file1&file2
$qs = $_SERVER["QUERY_STRING"];
if (isset($_SERVER["PATH_INFO"]) && "/"===substr($_SERVER["PATH_INFO"], 0, 1)) {
    $qs = substr($_SERVER["PATH_INFO"], 1);
}

$qs = explode("&", $qs);
$c = count($qs);
if (0===$c || (1===$c && ''===$qs[0])) {
    // no query string
    welcome();
}

$output = "";

$match = true;  // default when SERVE_ONLY_YUI is set to false
$aFiles = array();
foreach ($qs as $st) {
    if (SERVE_ONLY_YUI) {
        $match = preg_match('@^.{3,12}/build/.*/.*\.(js|css|png)$@', $st);
    }
    if (!$match || in_array($st, $aFiles, true)) {
        // file does not match the regular expression, or file has already been specified
        continue;
    }
    $aFiles[] = $st;
}

if (0 === count($aFiles)) {
    header("HTTP/1.0 404 Not Found");
    echo "comboloader: no asked file<br />";
    exit(0);
}

$apckey = APC_PREFIX . PCL_VERSION . '-' . md5( implode("/", $aFiles) );
$res = false;
if (USE_APC) {
    $aCached = apc_fetch($apckey, $res);
    if ($res) {
        // Found in cache
        $mime      = $aCached[0];
        $fCompress = $aCached[1];
        $output    = $aCached[5];
    }
}

if (!$res) {
    // the result is not in the cache, or apc is not available
    $fCompress = true;
    $nbFiles = 0;
    foreach ($aFiles as $st) {
        $filename = YUI_ROOT . $st;
        $filename = realpath($filename);

        if ( !( $filename!==false && is_file($filename) && is_readable($filename) ) ) {
            // file doesn't exist / is not a file / is not readable: skip it
            continue;
        }

        $content = file_get_contents($filename);

        if ("js" === substr($st, -2)) {
            // todo check
            $output .= "/* Start of $st */\n";
            $output .= $content;
            $output .= "\n/* End of $st */\n";
            $mime = "application/javascript";
            $nbFiles++;
        } elseif ("css" === substr($st, -3)) {
            $rewritten = "";
            if ( preg_match('@^.{3,12}/build/.*/.*\.css$@', $st) ) {
                $content = handle_yui_css($content, $st);
                $rewritten = "(rewritten)";
            } // if YUI css

            $output .= "/* Start of $st $rewritten */\n";
            $output .= $content;
            $output .= "\n/* End of $st $rewritten */\n";
            $mime = "text/css";
            $nbFiles++;
        } elseif ("png" === substr($st, -3)) {
            // todo check
            $output .= $content;
            $fCompress = false;
            $mime = "image/png";
            $nbFiles++;
        }
    }

    if (0 === $nbFiles) {
        // none of the asked files were found
        header("HTTP/1.0 404 Not Found");
        echo "comboloader: cannot find asked files";
        exit(0);
    }

    // prepare compression
    $lenGunzip = strlen($output);
    if ($fCompress) {
        // don't compress images
        $output = gzencode($output, 9, FORCE_GZIP);
    }
    $lenGzip  = strlen($output);

    if (USE_APC) {
        // store in the cache
        apc_store($apckey, array($mime, $fCompress, $aFiles, $lenGunzip, $lenGzip, $output), TTL_APC);
    }
}


// very good article about gzip / deflate : http://zoompf.com/2012/02/lose-the-wait-http-compression

header("Content-type: " . $mime);

// according to https://developers.google.com/speed/docs/best-practices/caching?hl=fr :
// It is important to specify one of Expires or Cache-Control max-age, and one of Last-Modified
// or ETag, for all cacheable resources. It is redundant to specify both Expires and
// Cache-Control: max-age, or to specify both Last-Modified and ETag.
// We prefer Expires over Cache-Control: max-age because it is is more widely supported

header("ETag: \"$apckey\"");
header("Expires: " . date("D, d M Y H:i:s", time() + 31536000)." GMT");
header("Cache control: public");

$accept = isset($_SERVER["HTTP_ACCEPT_ENCODING"]) ? $_SERVER["HTTP_ACCEPT_ENCODING"] : "";
if ($fCompress && (strstr($accept, "gzip") !== FALSE) ) {
    // Browser handles Gzip, and content can be compressed (we don't compress alreaady compressed images)
    header("Vary: Accept-Encoding");
    header("Content-Encoding: gzip");
    echo $output;
    exit(0);
} else {
    // Don't send gzipped. The output is still cached gzipped.
    if ($fCompress) {
        $output = gzdecode($output);
    }
    echo $output;
    exit(0);
}


function welcome()
{
    $url = "";
    $url .= ( ("443" === $_SERVER["SERVER_PORT"] || (isset($_SERVER["HTTPS"]) && "on" === $_SERVER["HTTPS"])) ? ("https") : ("http") ) . "://";
    $url .= $_SERVER["HTTP_HOST"];
    $url .= $_SERVER["REQUEST_URI"];

    $nErr = $nWarn = $nTest = 0;

    $version = "";
    if (WELCOME_VERBOSE>=2) {
        $version = "v" . PCL_VERSION;
    }

    if (WELCOME_VERBOSE>=1) {
        echo "phpcomboloader $version <a href='http://bitbucket.org/gbouthenot/phpcomboloader'>project homepage</a><br />";
    }

    if (WELCOME_VERBOSE>=10) {
        if (COMBO_URL === $url) {
            echo "<tt>COMBO_URL</tt> seems ok<br />";
        } else {
            echo "<b>Warning: </b>COMBO_URL should probably be <tt>$url</tt> in ini/conf.php<br />";
            $nWarn++;
        }
        $nTest++;
    }

    if (WELCOME_VERBOSE>=15) {
        if (function_exists("apc_fetch")) {
            echo "apc is available, ";
            if (USE_APC) {
                echo "and will be used.<br />";
            } else {
                echo "but won't be used (denied by <tt>ini/conf.php</tt>).<br />";
            }
        } else {
            echo "apc is not available. Consider installing it for high performance.<br />";
        }
        $nTest++;
    }

    if (WELCOME_VERBOSE>=20) {
        // scan extlib/ directory
        if (is_dir(YUI_ROOT)) {
            echo "scanning <tt>extlib/</tt><br />";
            echo "<ul>";
            $galFound = false;
            $dirCount = 0;
            foreach (new DirectoryIterator(YUI_ROOT) as $fileInfo) {
                if($fileInfo->isDot() || !$fileInfo->isDir()) { continue; }
                $fname = $fileInfo->getFilename();
                if (is_dir(YUI_ROOT . $fname . DIRECTORY_SEPARATOR . "build")) {
                    if (WELCOME_VERBOSE>=25) {
                        echo "<li><tt>$fname</tt> seems ok</tt></li>";
                    }
                    if ("yui3-gallery" === $fname) {
                        $galFound = true;
                    }
                } else {
                    if (WELCOME_VERBOSE>=23) {
                        if (SERVE_ONLY_YUI) {
                            echo "<li><tt>$fname</tt>: <b>error: </b> doesn't contain a <tt>build</tt> directory. This is required for YUI Library</li>";
                            $nErr++;
                        } else {
                            echo "<li><tt>$fname</tt>: doesn't contain a <tt>build</tt> directory. This is allowed to serve non-YUI files</li>";
                        }
                    }
                }
                $nTest++;
                $dirCount++;
            }
            echo "</ul>";

            if (0 === $dirCount) {
                echo "<b>Error: </b><tt>extlib/</tt> directory is empty. It should contain other directories.<br />";
                $nErr++;
            } elseif ($galFound) {
                if (WELCOME_VERBOSE>=21) {
                    echo "yui3-gallery found.<br />";
                }
            } else {
                if (WELCOME_VERBOSE>=21) {
                    echo "<b>Warning: </b>yui3-gallery not found. Gallery will not be available.<br />";
                    $nWarn++;
                }
            }
            $nTest++;
        } else {
            echo "<b>Error: </b><tt>extlib/</tt> directory doesn't exist<br />";
            $nErr++;
        }
        $nTest++;

    }

    if (WELCOME_VERBOSE>=10) {
        echo "$nTest test(s) terminated with $nErr error(s) and $nWarn warning(s).<br />";
    }

    exit(0);
}
